import React, { useState } from "react";
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";
import { v4 as uuid } from "uuid";
import Plot from "react-plotly.js";
import Button from "@mui/material/Button";
import { Dropdown } from "@mui/base/Dropdown";
import { Menu } from "@mui/base/Menu";
import { MenuButton } from "@mui/base/MenuButton";
import { MenuItem, menuItemClasses } from "@mui/base/MenuItem";
import { styled } from "@mui/system";
import { Widgets } from "@mui/icons-material";

// initialisation des plots
/*const itemsFromBackend = [
  {
    id: uuid(), //identification pour le graphique
    content: {
      type: "pie", // le type de graphique
      values: [2, 3, 4, 4],
      labels: ["Wages", "Operating expenses", "Cost of sales", "Insurance"], //labels pour les points de données
      textinfo: "label+percent", // indique les info qui doivent etre affichées sur le graphique
      textposition: "outside", // la position des etiquettes de texte
      automargin: true, // ajustement
    },
  },
  {
    id: uuid(),
    content: {
      type: "pie",
      values: [2, 3, 4, 4],
      labels: ["Wages", "Operating expenses", "Cost of sales", "Insurance"],
      textinfo: "label+percent",
      textposition: "outside",
      automargin: true,
    },
  },
];*/

// pour les  three columns grid
const columnsFromBackend = {
  [uuid()]: {
    name: "Modèles",
    items: [],
  },

  [uuid()]: {
    name: "Entreprise 1",
    items: [],
  },

  [uuid()]: {
    name: "Entreprise 2",
    items: [],
  },

  [uuid()]: {
    name: "Entreprise 3",
    items: [],
  },
};

//pour les deplacements des models
const onDragEnd = (result, columns, setColumns) => {
  if (!result.destination) return;
  const { source, destination } = result;
  if (source.droppableId !== destination.droppableId) {
    const sourceColumn = columns[source.droppableId];
    const destColumn = columns[destination.droppableId];
    const sourceItems = [...sourceColumn.items];
    const destItems = [...destColumn.items];
    const [removed] = sourceItems.splice(source.index, 1);
    destItems.splice(destination.index, 0, removed);
    setColumns({
      ...columns,
      [source.droppableId]: {
        ...sourceColumn,
        items: sourceItems,
      },

      [destination.droppableId]: {
        ...destColumn,
        items: destItems,
      },
    });
  } else {
    const column = columns[source.droppableId];
    const copiedItems = [...column.items];
    const [removed] = copiedItems.splice(source.index, 1);
    copiedItems.splice(destination.index, 0, removed);
    setColumns({
      ...columns,
      [source.droppableId]: {
        ...column,
        items: copiedItems,
      },
    });
  }
};

// declarer un usestate pour initialiser les cullones

function DragDrop() {
  const [columns, setColumns] = useState(columnsFromBackend);

  const [modelsVisible, setModelsVisible] = useState(false); // Masquer/afficher les models
  const createHandleMenuClick = (widget) => {
    return () => {
      console.log(`Clicked on ${widget}`);

      if (widget === "pie") {
        const newItem = [
          ...columns[Object.keys(columns)[0]].items,

          {
            id: uuid(),
            content: {
              type: "pie",
              values: [2, 3, 4, 4],
              labels: [
                "Wages",
                "Operating expenses",
                "Cost of sales",
                "Insurance",
              ],
              textinfo: "label+percent",
              textposition: "outside",
              automargin: true,
            },
          },
        ];

        setColumns({
          ...columns,
          [Object.keys(columns)[0]]: {
            ...columns[Object.keys(columns)[0]],
            items: newItem,
          },
        });

        setModelsVisible(true);
      }

      if (widget === "bar") {
        const newItem = [
          ...columns[Object.keys(columns)[0]].items,

          {
            id: uuid(),
            content: {
              x: ["giraffes", "orangutans", "monkeys"],
              y: [20, 14, 23],
              type: "bar",
            },
          },
        ];

        setColumns({
          ...columns,

          [Object.keys(columns)[0]]: {
            ...columns[Object.keys(columns)[0]],
            items: newItem,
          },
        });

        setModelsVisible(true);
      }
      if (widget === "bubble") {
        const newItem = [
          ...columns[Object.keys(columns)[0]].items,

          {
            id: uuid(),
            content:  {
              x: [0, 1, 2],
              y: [6, 10, 2],
              error_y: {
                type: 'data',
                array: [1, 2, 3],
                visible: true
              },
              type: 'scatter'
            }
          },
        ];

        setColumns({
          ...columns,
          [Object.keys(columns)[0]]: {
            ...columns[Object.keys(columns)[0]],
            items: newItem,
          },
        });

        setModelsVisible(true);
      }
    };
  };

  function PieChart({ item }) {
    return (
      <Plot
        data={[
          {
            type: item.content.type,
            values: item.content.values,
            labels: item.content.labels,
            textinfo: item.content.textinfo,
            textposition: item.content.textinfo,
            automargin: item.content.automargin,
          },
        ]}
        layout={{
          height: 300,
          width: 200,
          margin: { t: 0, b: 0, l: 0, r: 0 },
          showlegend: false,
        }}
      />
    );
  }

  function BarChart({ item }) {
    return (
      <Plot
        data={[
          {
            x: item.content.x,
            y: item.content.y,
            type: item.content.type,
          },
        ]}
        layout={{
          height: 230,
          width: 220,
          margin: { t: 0, b: 0, l: 0, r: 0 },
          showlegend: false,
        }}
      />
    );
  }

  function BubbleChart({ item }) {
    return (
      <Plot
        data={[
          {
            x: item.content.x,
            y: item.content.y,
            type: item.content.type,
          },
        ]}
        layout={{
          height: 230,
          width: 220,
          margin: { t: 0, b: 0, l: 0, r: 0 },
          showlegend: false,
        }}
      />
    );
  }

  function ChartRenderer({ item }) {
    const { type } = item.content;

    switch (type) {
      case "pie":
        return <PieChart item={item} />;
      case "bar":
        return <BarChart item={item} />;
      case "bubble":
        return <BubbleChart item={item} />;
      // Ajoutez d'autres cas pour d'autres types de graphiques
      default:
        return <div>Type de graphique non pris en charge : {type}</div>;
    }
  }

  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        height: "100%",
        margin: "20px",
      }}
    >
      <div>
        {/* Bouton pour afficher/masquer les modèles */}

        <Button
          sx={{ p: 3, height: 5 }}
          variant="contained"
          color="primary"
          onClick={() => setModelsVisible(!modelsVisible)}
        >
          {modelsVisible ? "Masquer les modèles" : "Afficher les modèles"}
        </Button>
      </div>

      <DragDropContext // on a utiliser la librairie  react-beautiful-dnd
        onDragEnd={(result) => onDragEnd(result, columns, setColumns)}
      >
        {Object.entries(columns).map(([columnId, column], index) => {
          {
            if (index === 0 && !modelsVisible) {
              return null;
            }
          }

          return (
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
              key={columnId}
            >
              <h2>{column.name}</h2>

              <div style={{ margin: 8 }}>
                <Droppable droppableId={columnId} key={columnId}>
                  {(provided, snapshot) => {
                    return (
                      <div
                        {...provided.droppableProps}
                        ref={provided.innerRef}
                        style={{
                          background: snapshot.isDraggingOver
                            ? "lightblue"
                            : "lightgrey",
                          padding: 4,
                          width: 250,
                          minHeight: 500,
                        }}
                      >
                        {column.items.map((item, index) => {
                          return (
                            <Draggable
                              key={item.id}
                              draggableId={item.id}
                              index={index}
                            >
                              {(provided, snapshot) => {
                                return (
                                  <div
                                    ref={provided.innerRef}
                                    {...provided.draggableProps}
                                    {...provided.dragHandleProps}
                                    style={{
                                      userSelect: "none",
                                      padding: 16,
                                      margin: "0 0 8px 0",
                                      minHeight: "50px",
                                      backgroundColor: snapshot.isDragging
                                        ? "#263B4A"
                                        : "#456C86",
                                      color: "white",
                                      ...provided.draggableProps.style,
                                    }}
                                  >
                                    <Button // suppression
                                      variant="contained"
                                      color="error"
                                      sx={{ p: 1.5, ml: 19, height: 3 }}
                                      onClick={() => {
                                        const newItems = [
                                          ...column.items.slice(0, index),

                                          ...column.items.slice(index + 1),
                                        ];

                                        setColumns({
                                          ...columns,

                                          [columnId]: {
                                            ...column,

                                            items: newItems,
                                          },
                                        });
                                      }}
                                    >
                                      X
                                    </Button>
                                    <ChartRenderer item={item} />
                                  </div>
                                );
                              }}
                            </Draggable>
                          );
                        })}

                        {provided.placeholder}
                      </div>
                    );
                  }}
                </Droppable>
              </div>
            </div>
          );
        })}
      </DragDropContext>

      <Dropdown>
        <TriggerButton color = "blue">Ajouter model</TriggerButton>

        <Menu slots={{ listbox: StyledListbox }}>
          <StyledMenuItem onClick={createHandleMenuClick("bubble")}>
            Bubble Chart
          </StyledMenuItem>

          <StyledMenuItem onClick={createHandleMenuClick("pie")}>
            Pie Chart
          </StyledMenuItem>

          <StyledMenuItem onClick={createHandleMenuClick("bar")}>
            Bar Chart
          </StyledMenuItem>
        </Menu>
        <div></div>
      </Dropdown>
    </div>
  );
}

const blue = {
  100: "#DAECFF",
  200: "#99CCF3",
  400: "#3399FF",
  500: "#007FFF",
  600: "#0072E5",
  900: "#003A75",
};

const grey = {
  50: "#f6f8fa",
  100: "#eaeef2",
  200: "#d0d7de",
  300: "#afb8c1",
  400: "#8c959f",
  500: "#6e7781",
  600: "#57606a",
  700: "#424a53",
  800: "#32383f",
  900: "#24292f",
};

const StyledListbox = styled("ul")(
  ({ theme }) => `
  font-family: IBM Plex Sans, sans-serif;
  font-size: 0.875rem;
  box-sizing: border-box;
  padding: 6px;
  margin: 12px 0;
  min-width: 200px;
  border-radius: 12px;
  overflow: auto;
  outline: 0px;
  background: ${theme.palette.mode === "dark" ? grey[900] : "#fff"};
  border: 1px solid ${theme.palette.mode === "dark" ? grey[700] : grey[200]};
  color: ${theme.palette.mode === "dark" ? grey[300] : grey[900]};
  box-shadow: 0px 4px 30px ${
    theme.palette.mode === "dark" ? grey[900] : grey[200]
  };
  z-index: 1;`
);

const StyledMenuItem = styled(MenuItem)(
  ({ theme }) => `
  list-style: none;
  padding: 8px;
  border-radius: 8px;
  cursor: default;
  user-select: none;
  &:last-of-type {
    border-bottom: none;
  }

  &.${menuItemClasses.focusVisible} {
    outline: 3px solid ${theme.palette.mode === "dark" ? blue[600] : blue[200]};
    background-color: ${theme.palette.mode === "dark" ? grey[800] : grey[100]};
    color: ${theme.palette.mode === "dark" ? grey[300] : grey[900]};
  }

  &.${menuItemClasses.disabled} {
    color: ${theme.palette.mode === "dark" ? grey[700] : grey[400]};
  }

 
  &:hover:not(.${menuItemClasses.disabled}) {
    background-color: ${theme.palette.mode === "dark" ? grey[800] : grey[100]};
    color: ${theme.palette.mode === "dark" ? grey[300] : grey[900]};
  }
  `
);

const TriggerButton = styled(MenuButton)(
  ({ theme }) => `
  font-family: IBM Plex Sans, sans-serif;
  font-size: 0.875rem;
  font-weight: 600;
  box-sizing: border-box;
  min-height: calc(1.5em + 22px);
  border-radius: 12px;
  padding: 8px 14px;
  line-height: 1.5;
  background: ${theme.palette.mode === "dark" ? grey[900] : "#fff"};
  border: 1px solid ${theme.palette.mode === "dark" ? grey[700] : grey[200]};
  color: ${theme.palette.mode === "dark" ? grey[300] : grey[900]};
  transition-property: all;
  transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1);
  transition-duration: 120ms;

  &:hover {
    background: ${theme.palette.mode === "dark" ? grey[800] : grey[50]};
    border-color: ${theme.palette.mode === "dark" ? grey[600] : grey[300]};
  }

  &:focus-visible {
    border-color: ${blue[400]};
    outline: 3px solid ${theme.palette.mode === "dark" ? blue[500] : blue[200]};
  }
  `
);

export default DragDrop;
